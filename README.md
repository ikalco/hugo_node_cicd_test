## Download

```git clone https://gitlab.com/ikalco/hugo_tella_cicd_test.git```

---

## Install Dependencies

```npm install```

---

## Serve for Development

```npm run serve```

---

## Build for production

```npm run build```

Make sure to copy over any files in these folders
- public
- resources
- themes/tella/assets/style.css

There is also a glitch with the hugo engine which causes it to incorrectly parse url strings.
To get around this you need to manually change any reference to a file in the HTML from
```"/js/custom.js"```
to
```"js/custom.js"```.

---

# Made by Ikalco
